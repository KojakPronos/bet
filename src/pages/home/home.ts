import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TeamPage } from '../team/team';
import {  ModalController } from 'ionic-angular';

import { Http } from '@angular/http';


import {Observable} from 'rxjs/Rx';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  testes:any;
  keys:any;
  matchDate:any;

  constructor(public navCtrl: NavController,private http: Http) {

  }

   ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');

      this.http.get("https://soccer.sportmonks.com/api/v2.0/livescores?api_token=re0tQXO6WTJrqjwdp4D6ib8ceWhDAjK2tR3FLVF7Oj9m2ZzxddyrVX0gaGHQ&include=localTeam,visitorTeam,league,league.country&tz=Europe/Paris")
      .subscribe(result => {
        //console.log('testes',testes);
        let test = result.json();
      //console.log(this.testes["data"]);
        this.testes = test["data"];
        var groups = Object.create(null);

        for (var i = 0; i < this.testes.length; i++) {
            var item = this.testes[i];
            
            if(!groups[item.league_id]) {
                groups[item.league_id] = {
                  "items":[],
                  "league_name":item.league.data.name,
                  "country":item.league.data.country.data.name,
                  "flag":item.league.data.country.data.extra.flag
                };
            }
            groups[item.league_id].items.push(item);
        }

        this.testes = groups;
        this.keys = Object.keys(groups);

        var output = [];

        for (var i = (this.keys.length - 1); i >= 0; i--)
          {
            for (var j = 1; j <= i; j++)
            {
              var index = j - 1;
              var index2 = j;
              var indexval = this.keys[index];
              var indexval2 = this.keys[index2];
              var groupval = groups[indexval];
              var groupval2 = groups[indexval2];
              console.log(groupval.league_name);
              if (groupval.country > groupval2.country)
              {
                    var temp = this.keys[index];
                    this.keys[index] = this.keys[index2];
                    this.keys[index2] = temp;
              } 
            } 
          } 
  
      });
  }

  openPage(localteam_id){
    console.log("team_id:"+localteam_id);
          this.navCtrl.push(TeamPage, {
            id: localteam_id
          });
        }

  getHM(hm){
    return hm.substring(0,5);
  }     

  changeDate(){
    console.log("match date : "+this.matchDate);
    let url = "https://soccer.sportmonks.com/api/v2.0/fixtures/date/"+this.matchDate+"?api_token=re0tQXO6WTJrqjwdp4D6ib8ceWhDAjK2tR3FLVF7Oj9m2ZzxddyrVX0gaGHQ&include=localTeam,visitorTeam,league,league.country&tz=Europe/Paris";
    console.log('url:'+url);
    this.http.get(url)
      .subscribe(result => {
        //console.log('testes',testes);
        let test = result.json();
      //console.log(this.testes["data"]);
        let ttt = test["data"];
        var groups = [];

        for (var i = 0; i < ttt.length; i++) {
            var item = ttt[i];
            
            if(!groups[item.league_id]) {
                groups[item.league_id] = {
                  "items":[],
                  "league_name":item.league.data.name,
                  "country":item.league.data.country.data.name,
                  //"flag":item.league.data.country.data.extra.flag
                };
            }
            groups[item.league_id].items.push(item);
        }

        this.testes = groups;
        console.log("testes : "+ this.testes);
        this.keys = Object.keys(groups);

        var output = [];

        for (var i = (this.keys.length - 1); i >= 0; i--)
          {
            for (var j = 1; j <= i; j++)
            {
              var index = j - 1;
              var index2 = j;
              var indexval = this.keys[index];
              var indexval2 = this.keys[index2];
              var groupval = groups[indexval];
              var groupval2 = groups[indexval2];
              console.log(groupval.league_name);
              if (groupval.country > groupval2.country)
              {
                    var temp = this.keys[index];
                    this.keys[index] = this.keys[index2];
                    this.keys[index2] = temp;
              } 
            } 
          } 
  
      });
  } 
}

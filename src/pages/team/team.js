var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Http } from '@angular/http';
/**
 * Generated class for the TeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TeamPage = /** @class */ (function () {
    function TeamPage(navCtrl, http) {
        this.navCtrl = navCtrl;
        this.http = http;
    }
    TeamPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad HomePage');
        this.http.get("https://soccer.sportmonks.com/api/v2.0/teams/543?api_token=re0tQXO6WTJrqjwdp4D6ib8ceWhDAjK2tR3FLVF7Oj9m2ZzxddyrVX0gaGHQ")
            .subscribe(function (testes) {
            //console.log('testes',testes);
            _this.testes = testes.json();
            //console.log(this.testes["data"]);
            _this.testes = _this.testes["data"];
        });
        // In this page will show team data but as u can see {id} must be team id when I click on first page ok ?
        // SO When i click for exemple at this : CSK Moskova => ID 453 => go to team page and show API with {id} = 453
    };
    TeamPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-team',
            templateUrl: 'team.html',
        }),
        __metadata("design:paramtypes", [NavController, Http])
    ], TeamPage);
    return TeamPage;
}());
export { TeamPage };
//# sourceMappingURL=team.js.map